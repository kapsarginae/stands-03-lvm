# Подготовка

yum install -y xfsdump vim

pvcreate /dev/sdb

vgcreate vg_root /dev/sdb

lvcreate -n lv_root -l +100%FREE /dev/vg_root

# Создадим на нем файловую систему и смонтируем его, чтобы перенести туда данные: 

mkfs.xfs /dev/vg_root/lv_root

mount /dev/vg_root/lv_root /mnt

# Этой командой скопируем все данные с / раздела в /mnt: 

xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt

for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done

chroot /mnt/

grub2-mkconfig -o /boot/grub2/grub.cfg

cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done

sed -i -e 's/rd.lvm.lv=VolGroup00\/LogVol00/rd.lvm.lv=vg_root\/lv_root/' /boot/grub2/grub.cfg

# нужно в файле /boot/grub2/grub.cfg заменить rd.lvm.lv=VolGroup00/LogVol00 на rd.lvm.lv=vg_root/lv_root

# reboot

lvremove /dev/VolGroup00/LogVol00

lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00

mkfs.xfs /dev/VolGroup00/LogVol00 

mount /dev/VolGroup00/LogVol00 /mnt 

xfsdump -J - /dev/vg_root/lv_root | xfsrestore -J - /mnt

for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done

chroot /mnt/

grub2-mkconfig -o /boot/grub2/grub.cfg

cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done

# Создаем зеркало

pvcreate /dev/sdc /dev/sdd 

vgcreate vg_var /dev/sdc /dev/sdd 

lvcreate -L 950M -m1 -n lv_var vg_var 

mkfs.ext4 /dev/vg_var/lv_var 

mount /dev/vg_var/lv_var /mnt

cp -aR /var/* /mnt/

umount /mnt

mount /dev/vg_var/lv_var /var 

echo "`blkid | grep var: | awk '{print $2}'` /var ext4 defaults 0 0" >> /etc/fstab 

#reboot

lvremove /dev/vg_root/lv_root

vgremove /dev/vg_root 

pvremove /dev/sdb

# выделяем /home

lvcreate -n LogVol_Home -L 2G /dev/VolGroup00

mkfs.xfs /dev/VolGroup00/LogVol_Home 

mount /dev/VolGroup00/LogVol_Home /mnt/ 

cp -aR /home/* /mnt/

rm -rf /home/*

umount /mnt 

mount /dev/VolGroup00/LogVol_Home /home/ 

echo "`blkid | grep Home | awk '{print $2}'` /home xfs defaults 0 0" >> /etc/fstab 

# выделяем /opt

lvcreate -n LogVol_Opt -L 8G /dev/VolGroup00

mkfs.xfs /dev/VolGroup00/LogVol_Opt 

mount /dev/VolGroup00/LogVol_Opt /mnt/ 

cp -aR /opt/* /mnt/

rm -rf /opt/*

umount /mnt 

mount /dev/VolGroup00/LogVol_Opt /opt/ 

echo "`blkid | grep Opt | awk '{print $2}'` /opt xfs defaults 0 0" >> /etc/fstab 

touch /home/file{1..20}

lvcreate -L 100MB -s -n home_snap /dev/VolGroup00/LogVol_Home

rm -f /home/file{11..20}

umount /home

lvconvert --merge /dev/VolGroup00/home_snap

mount /home

